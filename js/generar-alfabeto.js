// Ejemplo 1
function generaAlfabeto() {
    var n;
    n = parseInt(prompt("¿Cuántas letras desear añadir al alfabeto? (1-26)"));
    //console.log(n);

    // comprobamos que han introducido un numero
    if(isNaN(n) == true) {
        return ('¡Cancelado!');
    }

    // Comprobamos que se ha introducido un numero entre 1 y 26
    if(n < 1) {
        console.log('ha introducido un numero menor que 1');
        n = 1;
    }
    
    if(n > 26) {
        console.log('ha introducido un numero mayor que 26');
        n = 26;
    }
    var alfabeto = new Array (n);

    for(var i = 0; i < n; i++) {
        alfabeto[i] = String.fromCharCode(65 + i);
    }

    return alfabeto;

}

console.log(generaAlfabeto());
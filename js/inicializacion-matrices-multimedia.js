// var coches = new Array(3);

var coches = [];

coches[0] = "Toyota";
coches[1] = "Audi";
coches[2] = "Ford";

// INICIALIZACIÓN

// Mala Inicialización
// var cochesWapos = new Array('Porsche','Ferrari','Lamborghini');

// Correcta inicialización
var cochesWapos = ['Porsche','Ferrari','Lamborghini'];

for (cocheWapo in cochesWapos) {
    console.log(cochesWapos[cocheWapo]);
}

var miMatriz = ['cadena', 5, true];

console.log('string: ' + miMatriz[0]);
console.log('number: ' + miMatriz[1]);
console.log('booleano: ' + miMatriz[2]);

var dimension1 = ['00','01','02'];
var dimension2 = ['10','11','12'];
var matriz2D = [dimension1,dimension2];

console.log(matriz2D[0]);
console.log(matriz2D[0][0]);
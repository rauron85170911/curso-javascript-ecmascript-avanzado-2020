// procesado de matrices

var matriz8 = [1,2,3,4,5,6,7,8,9,10];

function esPar(n) {
    return (n % 2) == 0;
}

var numerosPares = matriz8.filter(esPar);

console.log(numerosPares);
console.log(matriz8);

function elDoble(n) {
    return n * 2;
}

var dobles = matriz8.map(elDoble);

console.log(dobles);
console.log(matriz8);

function mostrar(n,indice,matriz) {
    console.log("el elemento["+ indice +"] tiene el valor: "+ n);
}

var matriz9 = [1,3,5];
matriz9.forEach(mostrar);
// Fecha sin formato

var n = new Date();

console.log(n);

// formato fecha corta local

console.log(n.toLocaleDateString());

// formato hora corto local

console.log(n.toLocaleTimeString());


// excesos

var a = new Date(1972,12,23);

console.log(a);

var b = new Date(1972,3,40);

console.log(b);

var c = new Date(1972,3,-10);

console.log(c);

// formato cadena

var cadena = new Date('May 12, 2015 22:15:37');

console.log(cadena);

// comprobar si se manda una fecha en una variable

var f1 = new Date();
var f2 = 345;
var f3 = "Cualquier cosa";

function  esFecha(fecha) {
    return (fecha instanceof Date);
}

console.log(esFecha(f1));
console.log(esFecha(f2));
console.log(esFecha(f3));

// truco para sacar los dias que tiene un mes

function numDiasEnMes(mes, anio)
{
    var comodin = new Date(anio, mes+1, 0);
    return comodin.getDate();
}

console.log(numDiasEnMes(1, 2012));
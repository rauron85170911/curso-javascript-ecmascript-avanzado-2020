// simplificar codigo 

// antes 

res = (Math.exp(x) - Math.exp(x)) * Math.sin(y) / Math.PI * Math.log(z);

// despues

with (Math) {
    res = (exp(x) - exp(x)) * sin(y) / PI * log(z);
}

function calculoTrayectoria (x,y,z) {
    with (Math) {
        res = (exp(x) - exp(x)) * sin(y) / PI * log(z);
    }
}


// definición y uso de matrices

/*var coche1 = 'Toyota';
var coche2 = 'Audi';
var coche3 = 'Ford';*/

//var coches = new Array(3);

var coches = [];

coches[0] = "Toyota";
coches[1] = "Audi";
coches[2] = "Ford";

// acceso a la matriz

//pintamos el elemento 2
console.log(coches[1]);

//pintamos el numero de elementos totales en la matriz
console.log('El numero de elementos totales es: ' + coches.length);

//para redimensionar la  matriz
//coches.length = 7;

// si la matriz al llamar a un numero de elemtno es mayor de lso que estan creados dara un "undefined"
console.log(coches[6]);

// recorrer la matriz con un for

for (var i = 0; i < coches.length; i++){
    console.log('coche: ', coches[i]);
}

// bucle especifico para recorrer matrices
for (var coche in coches) {
    console.log('coche con for in: ' + coches[coche]);
}

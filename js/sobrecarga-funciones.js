// sobrecarga de funciones (usando arguments)

function sumarNumeros () {
    var parcial = 0;
    for (var i=0; i < sumarNumeros.arguments.length; i++) {

        var n = parseInt(sumarNumeros.arguments[i], 10);

        if (!isNaN(n)) {
            parcial += n;
        }

        //parcial += sumarNumeros.arguments[i];
    }
    return parcial;
}
console.log(sumarNumeros(1,2,3,4,5,"cualquier cosa","6",7,8,9));
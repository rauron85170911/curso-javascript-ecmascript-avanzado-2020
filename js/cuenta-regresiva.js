// cuenta regresiva

var dObjetivo = null;
var divreloj = null;
var idTimer = 0;

function comenzarParar(){
    if(idTimer !=0) { //Parar
        //paramos el temporizador
        clearInterval(idTimer);
        idTimer = 0;
        //cambiar el texto del botón "Parar"
        document.getElementById('botonCrono').value = "iniciar";
        //Poner el contador a cero
        pintarTiempo (0,0);
    
    }
    else { //Arrancar
        //Se averigua el tiempo seleccionado (son minutos);
        var tDescuento = parseInt(document.getElementById('tiempo').value,10);
        dObjetivo = new Date(); // hora actual como referencia
        dObjetivo.setMinutes(dObjetivo.getMinutes() + tDescuento); //sumamos el numero de minutos
        //cambiar texto del boton "Parar"
        document.getElementById('botonCrono').value = "Parar";
        //poner el timepo inicial
        pintarTiempo(tDescuento, 0);
        //lanzar temporizador
        idTimer = setInterval(tick,900);
    }
}

function tick() {
    //Se calcula el timepo entre la hora objetivo y la actual
    var dif = (dObjetivo - new Date()) / 1000; //dividimos entre 100 para obtener segundos
    if(dif > 0) {
        var minutos = Math.floor(dif / 60);
        var segundos = Math.floor(dif % 60);
        pintarTiempo(minutos,segundos);
    }
    else {
        comenzarParar(); // lo paramos
    }
}

function pintarTiempo(minutos,segundos) {
    if (divreloj == null){ divreloj = document.getElementById('reloj');}
    if(minutos.toString().length == 1){minutos = "0" + minutos;}
    if(segundos.toString().length == 1){segundos = "0" + segundos;}

    divreloj.textContent = minutos + ":" + segundos;

}
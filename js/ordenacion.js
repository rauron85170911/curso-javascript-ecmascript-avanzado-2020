// Ordenación
var matriz6 = [7,3,9,11,4];

// reverse
console.log(matriz6.reverse());

//short
console.log(matriz6.sort());

var frutas = ["Naranja","Pera","Melocoton","Fruta de la pasión","Uva"];

function compararFruta(f1,f2) {
    if(f1.length > f2.length){
        return 1;
    }else if(f1.length < f2.length) {
        return -1;
    }
    else{
        return 0;
    }
}

frutas.sort(compararFruta);
console.log(frutas);
//errores en modo estricto

"use strict"; // declaración

var miVar = 1;

function test() {
    
    mivar = 5;
    console.log(miVar);
}

test();

function test2 (p1,p2,p3) {
    console.log(p1);
    console.log(p2);
}

test2(1,2,3);

var objeto = {};

var objeto = {
    prop1 : "Hola",
    prop2 : "Adios",
    prop1 : "Otra cosa"
};

console.log(objeto.prop1);

eval("var x=5;");

console.log(x);

eval = "hola";

function test3(p1){
    p1 = "10";
    console.log(p1);
    console.log(arguments[0]);
}

test3(5);

// mdn use strict

// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Modo_estricto
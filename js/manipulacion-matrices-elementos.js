// Metodos - manipulación de elementos

// join
var mEjemplo = [0,1,2,3,4,5,6,7,8,9];

console.log(mEjemplo.join('--'));

// concat

var matriz1 = [1,2,3];
var matriz2 = [4,5,6];
var siete = 7;
var ocho = 'ocho';

var nuevaMatriz = matriz1.concat(matriz2, siete, ocho);

console.log(nuevaMatriz);

// slice

var miMatriz = [1,2,3,4];

var otraMatriz = miMatriz.slice();

otraMatriz[1] = 5;

console.log(miMatriz[1]);

// splice

var matriz3 = [1,2,3,4,5,6,8];

var res = matriz3.splice(3,2);

console.log(res);
console.log(matriz3);
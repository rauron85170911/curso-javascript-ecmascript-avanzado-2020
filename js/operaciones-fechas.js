// operaciones con fechas

var base = new Date(2010,0,1); 1/1/2010
var hoy = new Date();

// como restar fechas

var diferencia = hoy - base;

console.log(diferencia);

var difDias = diferencia / (24 * 60 * 60 * 1000);

//devuelve la diferencia en dias
console.log(difDias);

var difMeses = diferencia / (30 * 24 * 60 * 60 * 1000);

//devuelve la diferencia en meses
console.log(difMeses);

console.log(Date.parse(base));

// sumamos 5 dias

console.log(new Date(base.setDate(base.getDate() + 5)));

// sumamos 13 segundos

console.log(new Date(base.setSeconds(base.getSeconds() + 13)));

// sumamos 30000 milisegundos -> 30 segundos

console.log(new Date(base.setTime(base.getTime() + 30000)));


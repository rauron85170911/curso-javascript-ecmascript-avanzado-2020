// Matrices asociativas

// JavaScript no soporta matrices asociativas aunque este ejemplo lo parezca

var coches = [];

coches["Alemanes"] = ['Audi','Volkswagen','Porsche'];
coches["Franceses"] = ['Renault','Citroen'];
coches["Italianos"] = ['Fiat','Alfa Romeo','Ferrari'];

console.log(coches.Alemanes);
console.log(coches.Franceses);
console.log(coches.Italianos);

//Herencia

function Marcianito(elNombre,elColor,posX,posY,disparosIniciales) {
    this.name = elNombre;
    this.color = elColor;

    if (posX < 0) posX = 0;
    if (posX < 100) posX = 100;

    this.x= posX;

    if (posY < 0) posY = 0;
    if (posY < 100) posY = 100;

    this.y= posY;

    if(disparosIniciales < 0) disparosIniciales = 0; 
    if(disparosIniciales > 100) disparosIniciales = 100; 
    this.disparos = disparosIniciales;


    // Comprobamos que no esta definida la función
    if(!Marcianito.prototype.disparar) {
        // function especifica para el constructor Marcianito
        Marcianito.prototype.disparar = function() {
            this.disparos--;
            //codigo para pintar el disparo
            console.log(this.name + " ha disparado");
        }
    };
}

var marcianito1 = new Marcianito('Invasor 1',"Azul",100,20,30);
var marcianito2 = new Marcianito('Invasor 2',"Rojo",140,20,50);

//marcianito2.disparar();

//console.log(marcianito1.name);
//console.log(marcianito2.disparos);

//console.log(typeof(marcianito1));

//comprobamos el tipo
//console.log(marcianito1.constructor == Marcianito);

// instanceof
//console.log(marcianito1 instanceof Marcianito);
//console.log(marcianito1 instanceof Object);

// false
//console.log(marcianito1 instanceof Date);
//marcianito1.disparar();
//console.log(marcianito1.disparar == marcianito2.disparar);



/*String.prototype.isNumeric = function (){
    return !isNaN(parseFloat(this));
}

console.log("1234".isNumeric());
console.log("ABCD".isNumeric());*/

console.log(marcianito1);

function NaveEstelar(elNombre,elColor,posX,posY) {
    Marcianito.call(this,elNombre,elColor,posX,posY,30);
    this.tipo = "Nave Estelar";
}

NaveEstelar.prototype = new Marcianito();
NaveEstelar.prototype.constructor = NaveEstelar;

function NaveNodriza(elNombre,elColor,posX,posY) {
    Marcianito.call(this,elNombre,elColor,posX,posY,50);
    this.tipo = "Nave Nodriza";
}

NaveNodriza.prototype = new Marcianito();
NaveNodriza.prototype.constructor = NaveNodriza;

var marcianito3 = new NaveEstelar ('Invasor del espacio 3',"Verde",100,20);
var marcianito4 = new NaveNodriza ('Invasor del espacio 4',"Amarillo",400,10);

console.log(marcianito3);
console.log(marcianito3.tipo);

console.log(marcianito4);
console.log(marcianito4.tipo);
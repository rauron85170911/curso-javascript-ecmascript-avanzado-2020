/*var marcianito1 = new Object();

marcianito1.name = "invasor del espacio #1";
marcianito1.color = "Azul";
marcianito1.x = 100;
marcianito1.y = 20;
marcianito1.disparos = 30;

console.log(marcianito1.name);
console.log(marcianito1.disparos);

marcianito1.disparos++;

console.log(marcianito1.disparos);

marcianito1.disparar = function() {
    this.disparos--;
    console.log(this.name + " ha disparado");
    console.log(marcianito1.disparos);
}

marcianito1.disparar();*/

///// Instancia de objetos tradicional 2 - JSON

var marcianito1 = {
    name : "invasor del espacio #1",
    color : "Azul",
    x : 100,
    y : 20,
    disparos : 30,
    disparar : function() {
        console.log(marcianito1.disparos);
        this.disparos--;
        console.log(this.name + " ha disparado");
        console.log(marcianito1.disparos);
    }
}

console.log('Es un elemento de tipo ' + typeof(marcianito1));

console.log(marcianito1.name);
console.log(marcianito1.disparar());
// Variables privadas

function Marcianito(elNombre,elColor,posX,posY,disparosIniciales) {

    var that = this;
    // this.name = elNombre;
    // Se definen con guion bajo las variables de ambito privado
    var _name = elNombre;
    
    Marcianito.prototype.getName = function() {
        return _name.toUpperCase() + " (" + that.color + ")";
    };

    Marcianito.prototype.setName = function(nombre) {
        _name = nombre;
    };

    that.color = elColor;

    if (posX < 0) posX = 0;
    if (posX < 100) posX = 100;

    that.x= posX;

    if (posY < 0) posY = 0;
    if (posY < 100) posY = 100;

    that.y= posY;

    if(disparosIniciales < 0) disparosIniciales = 0; 
    if(disparosIniciales > 100) disparosIniciales = 100; 
    that.disparos = disparosIniciales;


    // Comprobamos que no esta definida la función
    if(!Marcianito.prototype.disparar) {
        // function especifica para el constructor Marcianito
        Marcianito.prototype.disparar = function() {
            that.disparos--;
            //codigo para pintar el disparo
            console.log(that.getName() + " ha disparado");
        }
    };
}

var marcianito1 = new Marcianito('Invasor 1',"Naranja",500,50,110);

// Accedemos al metodo del constructor para recoger la variable _name de ambito privado

//console.log(marcianito1.getName());

// Modificamos la variable privada _name a traves del metodo set definido en el constructor

//marcianito1.setName("Comecocos");
//console.log(marcianito1.getName());
//console.log(marcianito1);
var fNombre = marcianito1.getName;
console.log(fNombre());
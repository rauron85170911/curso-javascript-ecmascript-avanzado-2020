function Marcianito(elNombre,elColor,posX,posY,disparosIniciales) {
    this.name = elNombre;
    this.color = elColor;

    if (posX < 0) posX = 0;
    if (posX < 100) posX = 100;

    this.x= posX;

    if (posY < 0) posY = 0;
    if (posY < 100) posY = 100;

    this.y= posY;

    if(disparosIniciales < 0) disparosIniciales = 0; 
    if(disparosIniciales > 100) disparosIniciales = 100; 
    this.disparos = disparosIniciales;


    // Comprobamos que no esta definida la función
    if(!Marcianito.prototype.disparar) {
        // function especifica para el constructor Marcianito
        Marcianito.prototype.disparar = function() {
            this.disparos--;
            //codigo para pintar el disparo
            console.log(this.name + " ha disparado");
        }
    };
}

var marcianito1 = new Marcianito('Invasor 1',"Azul",100,20,30);
var marcianito2 = new Marcianito('Invasor 2',"Rojo",140,20,50);

//marcianito2.disparar();

//console.log(marcianito1.name);
//console.log(marcianito2.disparos);

//console.log(typeof(marcianito1));

//comprobamos el tipo
//console.log(marcianito1.constructor == Marcianito);

// instanceof
//console.log(marcianito1 instanceof Marcianito);
//console.log(marcianito1 instanceof Object);

// false
//console.log(marcianito1 instanceof Date);
marcianito1.disparar();
console.log(marcianito1.disparar == marcianito2.disparar);



String.prototype.isNumeric = function (){
    return !isNaN(parseFloat(this));
}

console.log("1234".isNumeric());
console.log("ABCD".isNumeric());